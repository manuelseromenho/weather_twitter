## Weather from Twitter

Simple Python app that communicates with the Twitter "standard search API" and by using the full text of tweets, 
try to recognize the generic weather for Portugal in the last 30 days (using keywords like ex:"rain, cold, warm").

This is a simple experiment and it's faraway from being accurate. The objective of this project was to 
explore the communication with an API using OAuth2 authentication and getting from http requests useful information.

For simplicity I'm only searching the Twitter user called "bestweather_pt" as 
it seems it's focused on weather in Portugal.  

#### To run
    - create .env file and add your "CLIENT_KEY" (API KEY) and CLIENT_SECRET (API SECRET KEY)
    - execute on terminal "python -m app"
