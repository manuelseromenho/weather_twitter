import base64
import requests
from datetime import datetime, timedelta


def get_date():
    date = datetime.today() - timedelta(days=30)
    return date.strftime("%Y-%m-%d")


def create_authentication_header(client_key, client_secret):
    """Creating authentication header

    Args:
        client_key:
        client_secret:
    """
    key_secret = '{}:{}'.format(client_key, client_secret).encode('ascii')

    b64_encoded_key = base64.b64encode(key_secret)
    b64_encoded_key = b64_encoded_key.decode('ascii')

    return b64_encoded_key


def build_request(client_key, client_credentials, auth_url):
    """Building request

    Args:
        client_key:
        client_credentials:
        auth_url:
    """

    b64_encoded_key = create_authentication_header(client_key, client_credentials)

    auth_headers = {
        'Authorization': 'Basic {}'.format(b64_encoded_key),
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    }
    auth_data = {'grant_type': 'client_credentials'}
    auth_resp = requests.post(auth_url, headers=auth_headers, data=auth_data)

    return auth_resp


def search_headers(access_token):
    """
    Args:
        access_token:
    """
    headers = {
        'Authorization': 'Bearer {}'.format(access_token)
    }
    return headers
