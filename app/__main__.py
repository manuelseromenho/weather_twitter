import os
from dotenv import load_dotenv
from app.testing_twitter import TwitterWeatherChecker
from app.utils import build_request, get_date
from app.constants import USER_ID

load_dotenv()


def run():
    client_key = os.environ.get("CLIENT_KEY")
    client_secret = os.environ.get("CLIENT_SECRET")
    base_url = 'https://api.twitter.com/'
    auth_url = '{}oauth2/token'.format(base_url)

    authentication_response = build_request(client_key, client_secret, auth_url)
    access_token = authentication_response.json()['access_token']   # Bearer token
    date = get_date()
    twitter_weather_check = TwitterWeatherChecker(access_token, base_url)
    twitter_weather_check.search_by_user_response_weather(USER_ID, date)


if __name__ == '__main__':
    run()
