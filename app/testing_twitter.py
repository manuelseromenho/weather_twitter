import requests
import json
from .utils import search_headers


class TwitterWeatherChecker:

	def __init__(self, access_token, base_url):
		self.headers = search_headers(access_token=access_token)
		self.base_url = base_url

	def search_response(self):
		"""Build search

		Return search response as json
		"""
		search_params = {
			'q': 'NASA',
			'result_type': 'recent',
			'count': 1
		}
		search_url = '{}1.1/search/tweets.json'.format(self.base_url)
		search_resp = requests.get(search_url, headers=self.headers, params=search_params)

		return json.loads(search_resp.content)['statuses']

	def search_by_tweet_id_response_weather(self, user_id):
		"""Search the tweet for keywords like "rain, humidity, sun" and print the possible weather
		"""

		search_params = {
			'id': user_id,
			'tweet_mode': 'extended',
		}
		search_url = '{}1.1/statuses/show.json'.format(self.base_url)
		search_resp = requests.get(search_url, headers=self.headers, params=search_params)

		item = json.loads(search_resp.content)
		matches = ['humido', 'humidade', 'chuva', 'aguaceiros', '#chuva']
		if any(x in item['full_text'].lower() for x in matches):
			print("In the day {} it rained.".format(item['created_at'][4:16]))

		matches = ['frio', '#frio', 'fria']
		if any(x in item['full_text'].lower() for x in matches):
			print("In the day {} it was cold.".format(item['created_at'][4:16]))

		matches = ['sol', 'quente', 'céu azul', 'temperaturas amenas.']
		if any(x in item['full_text'].lower() for x in matches):
			print("In the day {} it was sunny.".format(item['created_at'][4:16]))

	def search_by_user_response_weather(self, user_id, date):
		"""Search the tweets of an user regarding weather
		"""

		search_params = {
			'user_id': user_id,
			'since': date,
			'count': 50
		}
		search_url = '{}1.1/statuses/user_timeline.json'.format(self.base_url)
		search_resp = requests.get(search_url, headers=self.headers, params=search_params)

		items = json.loads(search_resp.content)
		for item in items:
			self.search_by_tweet_id_response_weather(item['id'])

	def search_response_weather(self, date):
		"""Build search

		Return search response as json
		"""

		search_params = {
			'lang': 'pt', #https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
			# 'q': 'meteorologia portugal',
			'q': 'bestweather_pt',
			'since': date,
			# 'result_type': 'recent',
			# 'geocode': '38.7663,-9.09784,500km',
			'count': 200
		}
		search_url = '{}1.1/search/tweets.json'.format(self.base_url)
		search_resp = requests.get(search_url, headers=self.headers, params=search_params)

		for item in json.loads(search_resp.content)['statuses']:
			self.search_by_tweet_id_response_weather(item['id'])

		return json.loads(search_resp.content)['statuses']

	def check_rate_limit_status(self):
		"""Check rate limit for the current time window

		Return rate limit
		"""
		url = '{}1.1/application/rate_limit_status.json'.format(self.base_url)
		search_resp = requests.get(url, headers=self.headers)

		return json.loads(search_resp.content)['resources']['search']
